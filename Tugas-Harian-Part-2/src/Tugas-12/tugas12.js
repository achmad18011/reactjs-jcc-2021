import React, {useState, useEffect} from "react"
import axios from "axios"

const Tugas12= () =>{
  const [daftarNilai, setDaftarNilai] =  useState([])
  const[input,setInput]=useState({
    name:"",
    course: "",
    score: 0
})
    const [fetchStatus, setFetchStatus] = useState(true)
  const [currentId, setCurrentId] =  useState(null)

  const handleChange = (event) => {
    let value = event.target.value
    let nama = event.target.name
    setInput({...input,[nama] : value})
}
  useEffect( () => {
    const fetchData = async () => {
      const result = await axios.get(`http://backendexample.sanbercloud.com/api/student-scores`)
      setDaftarNilai(result.data.map(x=>{ 
          return {id: x.id, name: x.name,course: x.course , score: x.score} }) )
    }
    if(fetchStatus){
        fetchData()
        setFetchStatus(false)
    }  
    
  }, [fetchStatus,setFetchStatus])

  

    const handleEdit = (event) => {
        let idData = parseInt(event.target.value)
        axios.get(`http://backendexample.sanbercloud.com/api/student-scores/${idData}`)
        .then((e)=>{
            let data = e.data
            setInput(data)
        })
        setCurrentId(idData)
    }

    const handleDelete = (event) => {
        let idData = parseInt(event.target.value)
        axios.delete(`http://backendexample.sanbercloud.com/api/student-scores/${idData}`)
        .then(()=>{
            setFetchStatus(true)
        })
    }

    const handleSubmit = (event) =>{
        event.preventDefault()

        let name = input.name
        let course = input.course
        let score = input.score
        if (currentId === null){
            axios.post(`http://backendexample.sanbercloud.com/api/student-scores`,{name, score,course})
            .then(() => {
                setFetchStatus(true)
            })
        }else {
            axios.put(`http://backendexample.sanbercloud.com/api/student-scores/${currentId}`, {name, score,course})
            .then(()=>{
                setFetchStatus(true)
            })
        }
        setInput({
            name:"",
            course: "",
            score: 0
        })
        setCurrentId(null)
    }
    const indexNilai = (event) => {
        if (event>=80){
            return "A"
        }else if (event<80 && event>=70) {
            return "B"
        }else if (event<70 && event>=60){
            return "C"
        }else if (event<60 && event>=50){
            return "D"
        }else{
            return "E"
        }
    }

  return(
    <>
      { daftarNilai !== null &&
        (<div style={{width: "70%", margin: "0 auto"}}>
          <h1>Daftar Peserta Lomba</h1>
          <table className="peserta-lomba">
            <thead>
              <tr>
                <th>No</th>
                <th>Nama</th>
                <th>Mata Kuliah</th>
                <th>Nilai</th>
                <th>Indeks</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
                {
                  daftarNilai.map((item, index)=>{
                    return(                    
                      <tr key={index}>
                        <td>{index + 1}</td>
                        <td>{item.name}</td>
                        <td>{item.course}</td>
                        <td>{item.score}</td>
                        <td>{indexNilai(item.score)}</td>
                        <td>
                            <button onClick={handleEdit} value={item.id}>edit</button>
                            <button onClick={handleDelete} value={item.id}>delete</button>
                        </td>
                      </tr>
                    )
                  })
                }
            </tbody>
          </table>
          <br/>
        <br/>
        <form onSubmit = {handleSubmit}>
            <label>Nama:</label>
            <input type="text" name="name" value={input.name} onChange={handleChange} required/>
            <br/>
            <label>Mata Kuliah:</label>
            <input type="text" name="course" value={input.course} onChange={handleChange} required/>
            <br/>
            <label>Nilai:</label>
            <input type="number" name="score" value={input.score} min = {0} max ={100} onChange={handleChange}/>
            <br/>
            <input type="submit" value="Submit"/>
        </form>
        </div>)
      }

    </>
  )
}

export default Tugas12