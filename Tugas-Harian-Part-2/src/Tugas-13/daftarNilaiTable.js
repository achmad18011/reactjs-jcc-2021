import React, {useState, useEffect, useContext} from "react"
import axios from "axios"
import { DaftarNilaiContext } from "./daftarNilaiContext"

const DaftarNilaiTable = () =>{
  const { daftarNilai, setDaftarNilai, input, setInput, fetchStatus, setFetchStatus, currentId, setCurrentId, functions } = useContext(DaftarNilaiContext)
  const { fetchData, functionDelete, functionSubmit, functionUpdate, functionEdit } = functions
  const handleChange = (event) => {
    let value = event.target.value
    let nama = event.target.name
    setInput({...input,[nama] : value})
}
  useEffect( () => {
    
    if(fetchStatus){
        fetchData()
        setFetchStatus(false)
    }  
    
  }, [fetchStatus,setFetchStatus])

  

    const handleEdit = (event) => {
        let idData = parseInt(event.target.value)
        functionEdit(idData)
    }

    const handleDelete = (event) => {
        let idData = parseInt(event.target.value)
        functionDelete(idData)
    }

    const handleSubmit = (event) =>{
        event.preventDefault()
        if (currentId === null){
            functionSubmit()
        }else {
            functionUpdate()
        }
        setInput({
            name:"",
            course: "",
            score: 0
        })
        setCurrentId(null)
    }
    const indexNilai = (event) => {
        if (event>=80){
            return "A"
        }else if (event<80 && event>=70) {
            return "B"
        }else if (event<70 && event>=60){
            return "C"
        }else if (event<60 && event>=50){
            return "D"
        }else{
            return "E"
        }
    }

  return(
    <>
      { daftarNilai !== null &&
        (<div style={{width: "70%", margin: "0 auto"}}>
          <h1>Daftar Peserta Lomba</h1>
          <table className="peserta-lomba">
            <thead>
              <tr>
                <th>No</th>
                <th>Nama</th>
                <th>Mata Kuliah</th>
                <th>Nilai</th>
                <th>Indeks</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
                {
                  daftarNilai.map((item, index)=>{
                    return(                    
                      <tr key={index}>
                        <td>{index + 1}</td>
                        <td>{item.name}</td>
                        <td>{item.course}</td>
                        <td>{item.score}</td>
                        <td>{indexNilai(item.score)}</td>
                        <td>
                            <button onClick={handleEdit} value={item.id}>edit</button>
                            <button onClick={handleDelete} value={item.id}>delete</button>
                        </td>
                      </tr>
                    )
                  })
                }
            </tbody>
          </table>
          <br/>
        <br/>
        </div>)
      }

    </>
  )
}

export default DaftarNilaiTable