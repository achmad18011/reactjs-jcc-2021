import React, { useContext } from "react"
import { DaftarNilaiContext } from "./daftarNilaiContext"

const DaftarNilaiForm = () => {
    const { daftarNilai, setDaftarNilai, input, setInput, fetchStatus, setFetchStatus, currentId, setCurrentId, functions } = useContext(DaftarNilaiContext)
    const { fetchData, functionDelete, functionSubmit, functionUpdate, functionEdit } = functions
    const handleChange = (event) => {
    let value = event.target.value
    let nama = event.target.name
    setInput({...input,[nama] : value})
    }
    const handleSubmit = (event) =>{
        event.preventDefault()
        if (currentId === null){
            functionSubmit()
        }else {
            functionUpdate()
        }
        setInput({
            name:"",
            course: "",
            score: 0
        })
        setCurrentId(null)
    }
    return (
        <>
        <form onSubmit = {handleSubmit}>
            <label>Nama:</label>
            <br/>
            <input type="text" name="name" value={input.name} onChange={handleChange} required/>
            <br/>
            <label>Mata Kuliah:</label>
            <br/>
            <input type="text" name="course" value={input.course} onChange={handleChange} required/>
            <br/>
            <label>Nilai:</label>
            <br/>
            <input type="number" name="score" value={input.score} min = {0} max ={100} onChange={handleChange}/>
            <br/>
            <input type="submit" value="Submit"/>
        </form>
        </>
    )
}

export default DaftarNilaiForm