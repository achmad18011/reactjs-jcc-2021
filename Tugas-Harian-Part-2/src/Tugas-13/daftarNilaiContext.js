import axios from "axios"
import React, { createContext, useState } from "react"

export const DaftarNilaiContext = createContext()

export const DaftarNilaiProvider = props => {
    const [daftarNilai, setDaftarNilai] =  useState([])
    const[input,setInput]=useState({
    name:"",
    course: "",
    score: 0
    })
    const [fetchStatus, setFetchStatus] = useState(true)
    const [currentId, setCurrentId] =  useState(null)
    const indexNilai = (event) => {
        if (event>=80){
            return "A"
        }else if (event<80 && event>=70) {
            return "B"
        }else if (event<70 && event>=60){
            return "C"
        }else if (event<60 && event>=50){
            return "D"
        }else{
            return "E"
        }}

    const fetchData = async () => {
        const result = await axios.get(`http://backendexample.sanbercloud.com/api/student-scores`)
        setDaftarNilai(result.data.map(x=>{ 
            let indexScore = indexNilai(x.score)
            return {id: x.id, name: x.name, course: x.course , score: x.score, indexNilai: indexScore} }) )
      }
      const fetchDataId = async (idData) => {
        const result = await axios.get(`http://backendexample.sanbercloud.com/api/student-scores/${idData}`)
        setInput( {id: result.id, name: result.name, course: result.course , score: result.score} ) 
        setCurrentId(idData)
      }

    const functionDelete = (idData) => {
        axios.delete(`http://backendexample.sanbercloud.com/api/student-scores/${idData}`)
        .then(()=>{
            setFetchStatus(true)
        })
    }
    const functionSubmit = () => {
        let name = input.name
        let course = input.course
        let score = input.score
        axios.post(`http://backendexample.sanbercloud.com/api/student-scores`,{name, score,course})
            .then(() => {
                setFetchStatus(true)
            })
    }
    const functionUpdate = () => {
        let name = input.name
        let course = input.course
        let score = input.score
        axios.put(`http://backendexample.sanbercloud.com/api/student-scores/${currentId}`, {name, score,course})
            .then(()=>{
                setFetchStatus(true)
            })
    }
    const functionEdit = (idData) => {
        axios.get(`http://backendexample.sanbercloud.com/api/student-scores/${idData}`)
        .then((e)=>{
            let data = e.data
            setInput(data)
        })
        setCurrentId(idData)
    }
    const functions = {
        fetchData,
        functionDelete,
        functionSubmit,
        functionUpdate,
        functionEdit,
        fetchDataId
    }

    return(
        <DaftarNilaiContext.Provider value = {{
            daftarNilai,
            setDaftarNilai,
            input,
            setInput,
            fetchStatus,
            setFetchStatus,
            currentId,
            setCurrentId,
            functions
        }}>
            {props.children}
        </DaftarNilaiContext.Provider>
    )
    }