import React from "react"
import { DaftarNilaiContext, DaftarNilaiProvider } from "./daftarNilaiContext"
import DaftarNilaiForm from "./daftarNilaiForm"
import DaftarNilaiTable from "./daftarNilaiTable"

const DaftarNilai = () => {
    return(
        <DaftarNilaiProvider>
            <DaftarNilaiTable/>
            <DaftarNilaiForm/>
        </DaftarNilaiProvider>
    )
}

export default DaftarNilai