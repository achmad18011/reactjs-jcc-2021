import React, {useState, useEffect, useContext} from "react"
import axios from "axios"
import { DaftarNilaiContext } from "../Tugas-13/daftarNilaiContext"
import { Link } from "react-router-dom"
import { useHistory, useParams } from "react-router"
import { Table, Tag, Space } from 'antd';
import { message, Button, notification } from 'antd';
import { DeleteOutlined, EditOutlined } from '@ant-design/icons';



const Tugas15Table = () =>{
  
  let history = useHistory()
  const { daftarNilai, setDaftarNilai, input, setInput, fetchStatus, setFetchStatus, currentId, setCurrentId, functions } = useContext(DaftarNilaiContext)
  const { fetchData, functionDelete, functionSubmit, functionUpdate, functionEdit, fetchDataId } = functions
  const handleChange = (event) => {
    let value = event.target.value
    let nama = event.target.name
    setInput({...input,[nama] : value})
}
const success = () => {
  message.success('Data Terhapus', 3);
};
  useEffect( () => {
    
    if(fetchStatus){
        fetchData()
        setFetchStatus(false)
    }  
    
  }, [fetchStatus,setFetchStatus])

  

    const handleEdit = (event) => {
        let idData = parseInt(event.currentTarget.value)
        history.push(`/create2/${idData}`)
        functionEdit(idData)
    }

    const handleDelete = (event) => {
        let idData = parseInt(event.currentTarget.value)
        functionDelete(idData)
        success()
    }

    const handleSubmit = (event) =>{
        event.preventDefault()
        if (currentId === null){
            functionSubmit()
        }else {
            functionUpdate()
        }
        setInput({
            name:"",
            course: "",
            score: 0
        })
        setCurrentId(null)
        history.push('/Tugas14')
    }
    const indexNilai = (event) => {
        if (event>=80){
            return "A"
        }else if (event<80 && event>=70) {
            return "B"
        }else if (event<70 && event>=60){
            return "C"
        }else if (event<60 && event>=50){
            return "D"
        }else{
            return "E"
        }
    }
    const columns = [
      {
        title: 'Nama',
        dataIndex: 'name',
        key: 'name',
      },
      {
        title: 'Mata Kuliah',
        dataIndex: 'course',
        key: 'course',
      },
      {
        title: 'Nilai',
        dataIndex: 'score',
        key: 'score',
      },
      {
        title: 'Indeks',
        dataIndex: 'indexNilai',
        key: 'indexNilai',
      },
      {
        title: 'Action',
        key: 'action',
        render: (text, index) => (
          <Space size="middle">
            <Button onClick={handleEdit} value={text.id}><EditOutlined /></Button>
            <Button style={{backgroundColor:"red"}}  onClick={handleDelete} value={text.id}><DeleteOutlined /></Button>
          </Space>
        ),
      },
    ]
    const data = daftarNilai

  return(
    <>
    <br/>
     <h1>Daftar Peserta Lomba</h1>
     <br/>
     <Link to="/create2" ><Button style={{color:"white", backgroundColor:"Blue", marginLeft:"350px"}}>Create Form</Button></Link>
     <br/>
    <Table columns={columns} dataSource={data} style={{color:"black",width:"50%", margin:"auto"}} />
    </>
      /* { daftarNilai !== null &&
        (<div style={{width: "70%", margin: "0 auto"}}>
          <h1>Daftar Peserta Lomba</h1>
          <Link to="/create" style={{ marginLeft: "100px", paddingBottom:"10px"}}><button >create form</button></Link>
          <table className="peserta-lomba">
            <thead>
              <tr>
                <th>No</th>
                <th>Nama</th>
                <th>Mata Kuliah</th>
                <th>Nilai</th>
                <th>Indeks</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
                {
                  daftarNilai.map((item, index)=>{
                    return(                    
                      <tr key={index}>
                        <td>{index + 1}</td>
                        <td>{item.name}</td>
                        <td>{item.course}</td>
                        <td>{item.score}</td>
                        <td>{indexNilai(item.score)}</td>
                        <td>
                            <button onClick={handleEdit} value={item.id}>edit</button>
                            <button onClick={handleDelete} value={item.id}>delete</button>
                        </td>
                      </tr>
                    )
                  })
                }
            </tbody>
          </table>
          <br/>
        <br/>
        </div>)
      }

    </> */
  )
}

export default Tugas15Table