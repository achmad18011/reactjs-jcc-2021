import react from 'react';
import './App.css';
// import Tugas9 from "./Tugas-9/tugas9"
// import Tugas10 from "./Tugas-10/tugas10"
// import Tugas11 from './Tugas-11/tugas11';
import Tugas12 from './Tugas-12/tugas12';
import DaftarNilai from './Tugas-13/daftarNilai';
import { DaftarNilaiProvider } from './Tugas-13/daftarNilaiContext';
import Routes from './Tugas-14/Routes';
import 'antd/dist/antd.css';
const App = () => {
  return (
    <>
      {/* <Tugas10/> */}
      {/* <Tugas9/> */}
      {/* <Tugas11/> */}
      {/* <Tugas12/> */}
      {/* <DaftarNilai/> */}
      <DaftarNilaiProvider>
            <Routes/>
        </DaftarNilaiProvider>
    </>
  );
}
export default App;
