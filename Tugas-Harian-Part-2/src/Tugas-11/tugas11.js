import React, { useState, useEffect } from 'react';

const Tugas11 = () => {
    const [daftarBuah, setDaftarBuah] = useState ([
        {nama: "Nanas", hargaTotal: 100000, beratTotal: 4000 },
        {nama: "Manggis", hargaTotal: 350000, beratTotal: 10000},
        {nama: "Nangka", hargaTotal: 90000, beratTotal: 2000},
        {nama: "Durian", hargaTotal: 400000, beratTotal: 5000},
        {nama: "Strawberry", hargaTotal: 120000, beratTotal: 6000}
      ])
    const[input,setInput]=useState({
        nama:"",
        hargaTotal: 0,
        beratTotal:0
    })
    const [currentIndex, setCurrentIndex] = useState(-1)

    const handleChange = (event) => {
        let value = event.target.value
        let nameOfChange = event.target.name
        setInput({...input, [nameOfChange] : value})
    }

    const handleSubmit = (e) => {
        e.preventDefault()
        let newData = daftarBuah
        const {nama, hargaTotal, beratTotal} = input
        if (currentIndex === -1){
            newData = [...daftarBuah,{nama, hargaTotal, beratTotal}]
        }else{
            newData[currentIndex] = {nama, hargaTotal, beratTotal}
        }
        
        setDaftarBuah(newData)
        setInput({
            nama:"",
            hargaTotal: 0,
            beratTotal:0
        })
        setCurrentIndex(-1)
    }

    const handleDelete = (event) => {
        let index = parseInt(event.target.value)
        let deletedItem = daftarBuah[index]
        let newData = daftarBuah.filter((e) => {
            return e !== deletedItem
        })
        setDaftarBuah(newData)
    }
    
    const handleEdit = (event) => {
        let index = parseInt(event.target.value)
        let editValue = daftarBuah[index]
        setInput({
            nama: editValue.nama,
            hargaTotal: editValue.hargaTotal,
            beratTotal: editValue.beratTotal
        })
        setCurrentIndex(event.target.value)
    }

      return <>
        <h1>Daftar Harga Buah</h1>
        <table>
            <thead>
                <tr>
                    <th>No.</th>
                    <th>Nama</th>
                    <th>Harga Total</th>
                    <th>Berat Total</th>
                    <th>Harga Per Kg</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
                {daftarBuah.map((val, index) => {
                    return(
                        <tr>
                            <td>{index + 1}</td>
                            <td>{val.nama}</td>
                            <td>{val.hargaTotal}</td>
                            <td>{val.beratTotal/1000} Kg</td>
                            <td>{val.hargaTotal/(val.beratTotal/1000)}</td>
                            <td>
                                <button onClick={handleEdit} value={index}>edit</button>
                                <button onClick={handleDelete} value={index}>delete</button>
                            </td>
                        </tr>
                    )
                })}
            </tbody>
        </table>
        <br/>
        <br/>
        <form onSubmit = {handleSubmit}>
            <label>Nama:</label>
            <input type="text" name="nama" value={input.nama} onChange={handleChange}/>
            <br/>
            <label>Harga total:</label>
            <input type="number" name="hargaTotal" value={input.hargaTotal} onChange={handleChange}/>
            <br/>
            <label>Berat Total(dalam gram):</label>
            <input type="number" name="beratTotal" value={input.beratTotal} min = {2000} onChange={handleChange}/>
            <br/>
            <input type="submit" value="Submit"/>
        </form>
      </>
}
export default Tugas11