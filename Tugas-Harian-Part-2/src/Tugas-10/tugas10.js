import React, { useState, useEffect } from 'react';

const Tugas10 = () => {
    const [count, setCount] = useState(100);
    const [date, setDate] = useState(new Date().toLocaleTimeString())
    const [kondisi, setKondisi] = useState("")
    const [kondisi2, setKondisi2] = useState("")
    useEffect(() => {
        setDate(new Date().toLocaleTimeString())
    })
    useEffect(() => {
        if (count > 0){
            setTimeout(()=>{
                setCount(count - 1)
            },1000)
        }   
    },[count])
    useEffect(() =>{
        if (count !== 0){
            setKondisi("Now At - " + date  )
            setKondisi2("Countdown : " + count)
        }else{
            setKondisi(null)
            setKondisi2(null)
        }
    })
    return ( <div>
        <h2 className="a">{kondisi}</h2>
        <h4 className="a">{kondisi2} </h4>
        </div> 
        );
}
export default Tugas10