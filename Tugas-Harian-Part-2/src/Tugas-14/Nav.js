import react, { useContext } from "react"
import { Link } from "react-router-dom"
import { ThemeContext} from "../Tugas-13/ThemeColor";
import ButtonSwitch from "./ButtonSwitch";


const Nav =()=>{
    const {theme, setTheme} = useContext(ThemeContext)
    const styleName = theme === "white" ? "white" : "black"
    return(
        <>
            <div className = {`box ${styleName}`}>
                <Link style={{borderRight:"1px solid grey",padding:"20px 20px",color:"grey", textDecoration:"none"}} to="/">Tugas 9</Link>
                <Link style={{borderRight:"1px solid grey",padding:"20px 20px",color:"grey", textDecoration:"none"}} to="/Tugas10">Tugas 10</Link>
                <Link style={{borderRight:"1px solid grey",padding:"20px 20px",color:"grey", textDecoration:"none"}} to="/Tugas11">Tugas 11</Link>
                <Link style={{borderRight:"1px solid grey",padding:"20px 20px",color:"grey", textDecoration:"none"}} to="/Tugas12">Tugas 12</Link>
                <Link style={{borderRight:"1px solid grey",padding:"20px 20px",color:"grey", textDecoration:"none"}} to="/Tugas13">Tugas 13</Link>
                <Link style={{borderRight:"1px solid grey",padding:"20px 20px",color:"grey", textDecoration:"none"}} to="/Tugas14">Tugas 14</Link>
                <Link style={{borderRight:"1px solid grey",padding:"20px 20px",color:"grey", textDecoration:"none"}} to="/Tugas15">Tugas 15</Link>
                <ButtonSwitch/>
            </div>
        </>
    )
}

export default Nav