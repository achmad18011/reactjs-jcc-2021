import React from "react"
import { BrowserRouter as Router,Switch, Route } from "react-router-dom"
import Nav from "./Nav"
import Tugas9 from "../Tugas-9/tugas9"
import Tugas10 from "../Tugas-10/tugas10"
import Tugas11 from "../Tugas-11/tugas11"
import Tugas12 from "../Tugas-12/tugas12"
import DaftarNilai from "../Tugas-13/daftarNilai"
import MahasiswaTable from "./daftarNilaiTable"
import MahasiswaForm from "./daftarNilaiForm"
import { DaftarNilaiProvider } from "../Tugas-13/daftarNilaiContext"
import { ThemeProvider } from "../Tugas-13/ThemeColor"
import Tugas15Table from "../Tugas-15/daftarNilaiTable"
import Tugas15Form from "../Tugas-15/daftarNilaiForm"

const Routes = () => {
    return(
        <>
        <ThemeProvider>
        <Router>
            <Nav/>

            <Switch>
                <Route exact path="/">
                    <Tugas9/>
                </Route>
                <Route exact path="/Tugas10">
                    <Tugas10/>
                </Route>
                <Route exact path="/Tugas11">
                    <Tugas11/>
                </Route>
                <Route exact path="/Tugas12">
                    <Tugas12/>
                </Route>
                <Route exact path="/Tugas13">
                    <DaftarNilai/>
                </Route>
                <Route exact path="/Tugas14">
                    <MahasiswaTable/>
                </Route>
                <Route exact path="/create">
                    <MahasiswaForm/>
                </Route>
                <Route exact path="/create/:value">
                    <MahasiswaForm/>
                </Route>
                <Route exact path="/Tugas15">
                    <Tugas15Table/>
                </Route>
                <Route exact path="/create2">
                    <Tugas15Form/>
                </Route>
                <Route exact path="/create2/:value">
                    <Tugas15Form/>
                </Route>

            </Switch>
        </Router>
        </ThemeProvider>
        </>
    )
}

export default Routes