import react, { useContext } from "react";
import { ThemeContext } from "../Tugas-13/ThemeColor";
import { Switch } from 'antd';
import { CloseOutlined, CheckOutlined } from '@ant-design/icons';

const ButtonSwitch = () =>{
    const {theme, setTheme} = useContext(ThemeContext)
    const handleButton = () => {
        setTheme(theme === "white" ? "black" : "white")
    }
    return(
        <>
            
            <Switch checkedChildren="Light Mode" unCheckedChildren="Dark Mode" defaultChecked onClick={handleButton} style={{float:"right", marginRight:"30px", marginTop:"12px"}}/>
        </>
    )
}
export default ButtonSwitch