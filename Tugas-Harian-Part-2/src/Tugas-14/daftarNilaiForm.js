import React, { useContext, useEffect } from "react"
import { useHistory, useParams } from "react-router"
import { DaftarNilaiContext } from "../Tugas-13/daftarNilaiContext"


const MahasiswaForm = () => {
    let history = useHistory()
    const { daftarNilai, setDaftarNilai, input, setInput, fetchStatus, setFetchStatus, currentId, setCurrentId, functions } = useContext(DaftarNilaiContext)
    const { fetchData, functionDelete, functionSubmit, functionUpdate, functionEdit, fetchDataId} = functions
    
    const handleChange = (event) => {
    let value = event.target.value
    let nama = event.target.name
    setInput({...input,[nama] : value})
    }
    let { value } = useParams()
    useEffect (()=>{
        if(fetchStatus){
            fetchDataId(value)
            setFetchStatus(false)
        }
    },[fetchDataId,fetchStatus,setFetchStatus])
    const handleSubmit = (event) =>{
        event.preventDefault()
        if (currentId === null){
            functionSubmit()
        }else {
            functionUpdate()
        }
        setInput({
            name:"",
            course: "",
            score: 0
        })
        setCurrentId(null)
        history.push('/Tugas14')
    }
    return (
        <>
        <h1>Daftar Nilai Form</h1>
        <form onSubmit = {handleSubmit}>
            <label>Nama:</label>
            <br/>
            <input type="text" name="name" value={input.name} onChange={handleChange} required/>
            <br/>
            <label>Mata Kuliah:</label>
            <br/>
            <input type="text" name="course" value={input.course} onChange={handleChange} required/>
            <br/>
            <label>Nilai:</label>
            <br/>
            <input type="number" name="score" value={input.score} min = {0} max ={100} onChange={handleChange}/>
            <br/>
            <input type="submit" value="Submit"/>
        </form>
        </>
    )
}

export default MahasiswaForm