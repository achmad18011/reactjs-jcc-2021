import React from "react";
import Nav from "./Nav";
import Cookies from "js-cookie"
import { Layout, Menu } from 'antd';
import { UnorderedListOutlined, HomeOutlined } from '@ant-design/icons';
import { Link, useHistory } from "react-router-dom"
const { SubMenu } = Menu;
const { Header, Content, Sider, Footer } = Layout;

const LayoutComponent = (props) => {

    return (
        <>

            <Layout>
            <Nav />
            <Layout>
            {Cookies.get('token') !== undefined ? <Sider width={200} className="site-layout-background">
                <Menu
                  mode="inline"
                  defaultSelectedKeys={['1']}
                  defaultOpenKeys={['sub1']}
                  style={{ height: '100%', borderRight: 0 }}
                >
                  <SubMenu key="sub1" icon={<HomeOutlined />} title="Dasbord">
                    <Menu.Item key="1"><Link to="/">Dasbord</Link></Menu.Item>
                  </SubMenu>
                  <SubMenu key="sub2" icon={<UnorderedListOutlined />} title="List">
                    <Menu.Item key="2"><Link to="/MovieList">Movie List</Link></Menu.Item>
                    <Menu.Item key="3"><Link to="/GameList">Game List</Link></Menu.Item>
                  </SubMenu>
                </Menu>
            </Sider> : null}
            <div className="row">
                <div className="section">
                    {props.content}
                </div>
            </div>
            </Layout>
            <Footer style={{ textAlign: 'center' }}>JabarCodingCamp ©2021 Created by Ach. Zanadin Zidan</Footer>
            </Layout>
        </>
    )

}

export default LayoutComponent