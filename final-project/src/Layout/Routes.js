import React, { useContext } from "react"
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from "react-router-dom"
import Nav from "./Nav"
import { MovieProvider } from '../Context/MovieContext';
import Movies from '../Pages/Movies';
import Games from '../Pages/Games';
import { GamesProvider } from '../Context/GamesContext';
import MoviesDescription from '../Pages/MoviesDescription';
import GamesDescription from '../Pages/GamesDescription';
import Login from '../Pages/Login';
import { UserContext, UserProvider } from '../Context/UserContext';
import Cookies from "js-cookie"
import Register from '../Pages/Register';
import MovieList from '../Pages/MovieList';
import { Link, useHistory } from "react-router-dom"
import GameList from '../Pages/GameList';
import MovieForm from '../Pages/MovieForm';
import GameForm from '../Pages/GameForm';
import GantiPassword from '../Pages/GantiPassword';
import LayoutComponent from './Layout';


const Routes = () => {
    const {loginStatus, setLoginStatus} = useContext(UserContext)
    const LoginRoute = ({...props}) => {
      if (Cookies.get('token') !== undefined){
        return <Redirect to="/"/>
      }else{
        return <Route {...props}/>
      }
    }
    return (
        <>
        
        <MovieProvider>
          <GamesProvider>
            <Router>
          
        
        <Switch>
          <Route path="/" exact>
              <LayoutComponent content={<Movies/>}/>
          </Route>
          <Route path="/Movie/:value" exact>
            <LayoutComponent content={<MoviesDescription/>}/>
          </Route>
          <Route path="/Games" exact>
              <LayoutComponent content={<Games/>}/>
          </Route>
          <Route path="/Games/:value" exact>
              <LayoutComponent content={<GamesDescription/>}/>
          </Route>
          <LoginRoute path="/Login" exact>
              <LayoutComponent content={<Login/>}/>
          </LoginRoute>
          <LoginRoute path="/Register" exact>
              <LayoutComponent content={<Register/>}/>
          </LoginRoute>
          <Route path="/MovieList" exact>
              <LayoutComponent content={<MovieList/>}/>
          </Route>
          <Route path="/MovieForm" exact>
              <LayoutComponent content={<MovieForm/>}/>
          </Route>
          <Route path="/MovieForm/:Id" exact>
              <LayoutComponent content={<MovieForm/>}/>
          </Route>
          <Route path="/GameList" exact>
              <LayoutComponent content={<GameList/>}/>
          </Route>
          <Route path="/GameForm" exact>
              <LayoutComponent content={<GameForm/>}/>
          </Route>
          <Route path="/GameForm/:Id" exact>
              <LayoutComponent content={<GameForm/>}/>
          </Route>
          <Route path="/GantiPassword" exact>
              <LayoutComponent content={<GantiPassword/>}/>
          </Route>
        </Switch>
        </Router>
          
          </GamesProvider>
        </MovieProvider>
        
        </>
    )
}

export default Routes