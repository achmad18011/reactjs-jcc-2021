import { Layout, Menu, Breadcrumb } from 'antd';
import { UserOutlined } from '@ant-design/icons';
import { Link, useHistory } from "react-router-dom"
import { useContext } from 'react';
import { UserContext } from '../Context/UserContext';
import Cookies from "js-cookie"
import Minos from "../Minos.png"

const { SubMenu } = Menu;
const { Header, Content, Sider, Footer } = Layout;

const Nav = () => {
    let history = useHistory()
    const {loginStatus, setLoginStatus} = useContext(UserContext)
    const handleLogout = () => {
        setLoginStatus(false)
        Cookies.remove('token')
        Cookies.remove('name')
        Cookies.remove('email')
        history.push('/Login')
    }

    
    return (
        <>
            <Header className="header">
            <img style={{width:"90px", maxHeight:"60px", margin:"0"}} src={Minos}/>
            <div className="logo" />
                <Menu theme="dark" mode="horizontal" defaultSelectedKeys={['6']}>
                <Menu.Item key="1" style={{position:"absolute",left:"160px"}}><Link to="/">Movies</Link></Menu.Item>
                <Menu.Item key="2" style={{position:"absolute",left:"240px"}}><Link to="/Games">Games</Link></Menu.Item>
                 <SubMenu key="sub1" icon={<UserOutlined/>} title="Account" style={{position:"absolute", right:"0"}}>
                    {Cookies.get('token') === undefined ?<Menu.Item key="5" ><Link to="/Login">Login</Link></Menu.Item>:<Menu.Item key="3" onClick={handleLogout}><span>Logout</span></Menu.Item> } 
                    {Cookies.get('token') === undefined ? <Menu.Item key="6" ><Link to="/Register">Register</Link></Menu.Item> :<Menu.Item key="4" ><Link to="/GantiPassword">Change Password</Link></Menu.Item>} 
                </SubMenu>
                
                
                </Menu>
                
            </Header>
        </>
    )
}

export default Nav