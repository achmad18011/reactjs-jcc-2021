import logo from './logo.svg';
import './App.css';
import 'antd/dist/antd.css';
import Routes from './Layout/Routes';
import { GamesProvider } from './Context/GamesContext';
import { UserContext, UserProvider } from './Context/UserContext';
import { MovieProvider } from './Context/MovieContext';

function App() {
  return (
    <>
          <UserProvider>
          <Routes/>
          </UserProvider>
    </>
  );
}

export default App;
