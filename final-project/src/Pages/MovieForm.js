import React, { useContext, useEffect } from "react"
import { useHistory, useParams } from "react-router"
import { MovieContext } from "../Context/MovieContext"

const MovieForm = () => {
    let history = useHistory()
    let {Id} = useParams()
    const { data, setData, fetchStatus, setFetchStatus, currentId, setCurrentId, input, setInput, functions } = useContext(MovieContext)
    const { functionDelete,
        functionEdit,
        fetchData,
        fetchDataId,
        handleChange,
        functionSubmit,
        functionUpdate } = functions
       
        useEffect(() => {
            if(Id !== undefined){
                functionEdit(Id)
            }
        },[])
        const handleSubmit = (e) => {
            e.preventDefault()
            if (currentId === null) {
               functionSubmit()
            }else{
               functionUpdate(currentId)
            }
            setInput({
                description: "",
                duration: 0,
                image_url: "",
                title: "",
                review: "",
                rating: 0,
                year: 1980,
                genre: ""
            })
            setCurrentId(null)
            history.push('/MovieList')
        }
        return (
            <>
                <div className="form">
                    <h1 style={{textAlign:"center"}}>Form Movie</h1>
                    <form style={{width:"60%", margin:"auto"}}method="post" onSubmit={handleSubmit}>
                        <label >Title</label>
                        <input onChange={handleChange} required className="input-form" type="text" id="fname" name="title"  value={input.title} />

                        <label >Genre</label>
                        <input onChange={handleChange} required className="input-form" type="text" id="fname" name="genre" value={input.genre} />
    
                        <label >Duration</label>
                        <input onChange={handleChange} required className="input-form" type="text" id="fname" name="duration" value={input.duration} />
    
                        <label >Description</label>
                        <textarea onChange={handleChange} required className="input-form" type="text" id="lname" name="description"  value={input.description} />
    
                        <label >year</label>
                        <input onChange={handleChange} required className="input-form" type="number" id="lname" name="year" min={1980} max={2021} value={input.year} />
    
                        <label >rating</label>
                        <input onChange={handleChange} required className="input-form" type="number" id="lname" name="rating" min={0} max={10} value={input.rating} />
    
                        <label >Review</label>
                        <textarea onChange={handleChange} required className="input-form" type="text" id="lname" name="review" value={input.review} />
    
                        <label >img url</label>
                        <input onChange={handleChange} required className="input-form" type="text" id="lname" name="image_url" value={input.image_url} />

                        <br />
                        <input className="input-submit" type="submit" value="Submit" />
                    </form>
                </div>
    
            </>)
}

export default MovieForm