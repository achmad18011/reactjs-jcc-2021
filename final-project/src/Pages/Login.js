import axios from "axios"
import Cookies from "js-cookie"
import React, { useContext, useState } from "react"
import { useHistory } from "react-router"
import {UserContext} from "../Context/UserContext"

const Login = () =>{
    let history = useHistory()
    const {userLists, setUserLists, loginStatus, setLoginStatus} = useContext(UserContext)
    const [input, setInput] = useState({
        email:"",
        password:""
    })

    const handleChange = (e) => {
        let typeOfInput = e.target.value
        let name = e.target.name
        setInput({...input, [name]: typeOfInput})
    }
    const handleSubmit = (e) => {
        e.preventDefault()
        axios.post(`https://backendexample.sanbersy.com/api/user-login`, {
            email:input.email,
            password:input.password
        }).then((res)=>{
            let token = res.data.token
            let user = res.data.user
            Cookies.set('token', token, {expires : 1})
            history.push('/')
        })
        setLoginStatus(true)
    }
  return(
    <>
        <div className="form">
            <h1 style={{textAlign:"center"}}>LOGIN</h1>
            <form style={{width:"30%", margin:"auto"}}method="post" onSubmit={handleSubmit}>
                <label > Email</label>
                <input onChange={handleChange} required className="input-form" type="text" id="fname" name="email"  value={input.email} />
    
                <label >Password</label>
                <input onChange={handleChange} required className="input-form" type="password" id="fname" name="password" value={input.password} />
                <br />

                <input className="input-submit" type="submit" value="Login" />
            </form>
        </div>
    </>
  )
}

export default Login