import React, { useContext, useEffect } from "react";
import { Card } from 'antd';
import { useHistory, useParams } from "react-router"
import { GameContext } from "../Context/GamesContext";
import { CloseSquareTwoTone, CheckSquareTwoTone } from '@ant-design/icons';


const GamesDescription = () =>{
    let history = useHistory()
    const { data, setData,dataDescription, setDataDescription, fetchStatus, setFetchStatus, currentId, setCurrentId, input, setInput, functions } = useContext(GameContext)
    const { fetchData, fetchDataId } = functions
    const {value} = useParams()

    useEffect(() => {

        if(fetchStatus){
            fetchDataId(value)
            setFetchStatus(false)
        }

    },[fetchStatus, setFetchStatus])
    const gridStyle = {
        width: '50%',
      };


    return (
        <>
        <div className="site-card-border-less-wrapper">
            <Card bordered={false} style={{ width: 1000 }}>
            <Card.Grid hoverable={false} style={gridStyle}>
                <img className="fakeimg" style={{ width: "100%", height: "100%", objectFit: "cover" }} src={dataDescription.image_url} /> 
            </Card.Grid>
            <Card.Grid hoverable={false} style={gridStyle}>
            <h2>{dataDescription.name}</h2>
                <h5>Release Year : {dataDescription.release}</h5>
                <p><b>Platform :</b> {dataDescription.platform}</p>
                <p><b>Genre :</b> {dataDescription.genre}</p>
                <p><b>SinglePlayer :</b> {dataDescription.singlePlayer === 0 ? <CloseSquareTwoTone /> : <CheckSquareTwoTone />}</p>
                <p><b>MultiPlayer :</b> {dataDescription.multiplayer  === 0 ? <CloseSquareTwoTone /> : <CheckSquareTwoTone /> }</p>
            </Card.Grid>
            </Card>
        </div>
                                 
        </>
    )
}
export default GamesDescription