import React, { useContext, useEffect, useState } from "react"
import { Link, useHistory } from "react-router-dom"
import { Table, Tag, Space, Button, Input, Layout } from 'antd';
import { MovieContext } from "../Context/MovieContext";
import { PlusOutlined, EditOutlined, DeleteOutlined } from '@ant-design/icons';
import axios from "axios";

const MovieList = () =>{

    let history = useHistory()
    const { functions } = useContext(MovieContext)
    const { functionDelete,
        functionEdit,
        fetchDataId,
        handleChange,
        functionSubmit,
        functionUpdate } = functions
    const [ data, setData] = useState([])
    const [fetchStatus, setFetchStatus] = useState(true)
    const [filter, setFilter] = useState({
        duration:null,
        year:null,
        rating: null 
    })

    const fetchData = async () => {
        let result = await axios.get(`https://backendexample.sanbersy.com/api/data-movie`)
        let fetchResult = result.data
        console.log(fetchResult)
        setData(
            fetchResult.map((res) => {
                return {
                    id: res.id,
                    description: res.description,
                    duration: res.duration,
                    image_url: res.image_url,
                    title: res.title,
                    review: res.review,
                    rating: res.rating,
                    year: res.year,
                    genre: res.genre
                }

            })
        )
    }
    useEffect(() => {

        if(fetchStatus){
            fetchData()
            setFetchStatus(false)
        }

    },[fetchData,fetchStatus,setFetchStatus])

    const handleEdit = (e) => {
        let idMobile = parseInt(e.currentTarget.value)
        history.push(`/MovieForm/${idMobile}`)
    
    }

    const handleDelete = (e) => {
        let idMobile = parseInt(e.currentTarget.value)

        functionDelete(idMobile)
    }
    const { Search } = Input;
    const columns = [
        {
          title: 'Title',
          dataIndex: 'title',
          key: 'title',
        },{
            title: 'Duration',
            dataIndex: 'duration',
            key: 'duration',
          },
        {
            title: 'Release Year',
            dataIndex: 'year',
            key: 'year',
          },
          {
            title: 'Genre',
            dataIndex: 'genre',
            key: 'genre',
          },
          {
            title: 'Rating',
            dataIndex: 'rating',
            key: 'rating',
          },
          {
            title: 'Review',
            dataIndex: 'review',
            key: 'review',
          },
          {
            title: 'Description',
            dataIndex: 'description',
            key: 'description',
          },
          {
            title: 'Image',
            dataIndex: 'image_url',
            key: 'image_url',
            render: text => <img src={text} style={{maxHeight:"120px", maxWidth:"120px"}}/>
          },
          {
            title: 'Action',
            key: 'action',
            render: (text, record) => (
              <Space size="middle">
                <Button onClick={handleEdit} value={text.id}><EditOutlined/></Button>
                <Button type="primary" onClick={handleDelete} value={text.id}><DeleteOutlined/></Button>
              </Space>
            ),
          },
    ]
    const datas = data
    const onSearch = value => {
        let fetchSearch = async() => {
            let result = await axios.get(`https://backendexample.sanbersy.com/api/data-movie`)
            let data = result.data

            let filterResult = data.filter((e)=>{
                return (Object.values(e).join(" ").toLowerCase().includes(value.toLowerCase()))
            })
            setData(filterResult)
        }
        fetchSearch()
    };

    const handleFilter = (e) => {
        e.preventDefault()
        console.log(filter)
        let fetchFilter = async () => {
            let result = await axios.get(`https://backendexample.sanbersy.com/api/data-movie`)
            let data = result.data
            console.log(data)
            let filterData = data.filter((e)=>{
                return e.duration == filter.duration || e.year == filter.year || e.rating == filter.rating
            })
            setData(filterData)
        }
        fetchFilter()
    }
    const handleChangeFilter = (e) => {
        let valueType = e.target.value
        let name = e.target.name

        setFilter({...filter, [name]:valueType})
    }

    return (
        <>
            <Layout>
                <h1 style={{textAlign:"center",fontSize:"large"}}>MOVIE LIST</h1>
                <br/>
                <div>
                    <form style={{marginLeft:"60px"}} onSubmit={handleFilter}>
                        <Input type="number" onChange={handleChangeFilter} value={filter.duration} name="duration" placeholder="Duration" style={{width:"150px",marginRight:"20px"}}/>
                        <Input type="number" onChange={handleChangeFilter} value={filter.year} name="year" placeholder="Release Year" style={{width:"150px",marginRight:"20px"}}/>
                        <Input type="number" onChange={handleChangeFilter} value={filter.rating} name="rating" placeholder="Rating" style={{width:"150px",marginRight:"20px"}}/>
                        <input type="submit" value="Filter"/>
                    </form>
                    <button style={{marginLeft:"60px"}} onClick={()=>{
                            setFetchStatus(true)
                            setFilter({
                                duration:null,
                                year:null,
                                rating: null  
                            })
                        }}>Reset Filter</button>
                </div>
                <br/>
                <Search style={{width:"25%",marginLeft:"60px"}} placeholder="input search text" allowClear enterButton="Search" onSearch={onSearch}/>
                <br/>
                <Button style={{position:"absolute",width:"150px", right:"60px", marginTop:"140px"}}><Link to="/MovieForm"><PlusOutlined /> Add Data Movie</Link></Button>
            <Table columns={columns} dataSource={datas} style={{width:"90%", margin:"auto"}}/>
            </Layout>
        </>
    )
}
export default MovieList