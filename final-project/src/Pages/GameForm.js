import React, { useContext, useEffect } from "react"
import { useHistory, useParams } from "react-router"
import { GameContext } from "../Context/GamesContext"


const GameForm = () => {
    let history = useHistory()
    let {Id} = useParams()
    const { data, setData, fetchStatus, setFetchStatus, currentId, setCurrentId, input, setInput, functions } = useContext(GameContext)
    const { functionDelete,
        functionEdit,
        fetchData,
        fetchDataId,
        handleChange,
        functionSubmit,
        functionUpdate } = functions
       console.log(input)
        useEffect(() => {
            if(Id !== undefined){
                functionEdit(Id)
            }
        },[])
        const handleSubmit = (e) => {
            e.preventDefault()
            if (currentId === null) {
               functionSubmit()
            }else{
               functionUpdate(currentId)
            }
            setInput({
                name:"",
                genre:"",
                singlePlayer:true,
                multiplayer:true,
                platform:"",
                release:2000,
                image_url:""
            })
            setCurrentId(null)
            history.push('/GameList')
        }
        return (
            <>
                <div className="form">
                    <h1 style={{textAlign:"center"}}>Form Game</h1>
                    <form style={{width:"60%", margin:"auto"}}method="post" onSubmit={handleSubmit}>
                        <label >Name</label>
                        <input onChange={handleChange} required className="input-form" type="text" id="fname" name="name"  value={input.name} />
    
                        <label >Genre</label>
                        <input onChange={handleChange} required className="input-form" type="text" id="fname" name="genre" value={input.genre} />
    
                        <label >Platform</label>
                        <input onChange={handleChange} required className="input-form" type="text" id="lname" name="platform"  value={input.platform} />
    
                        <label >Release Year</label>
                        <input onChange={handleChange} required className="input-form" type="number" id="lname" name="release" min={2000} max={2021} value={input.release} />
    
    
                        <label >img url</label>
                        <input onChange={handleChange} required className="input-form" type="text" id="lname" name="image_url" value={input.image_url} />

                    <label style={{ display: "block", width: "150px" }}>
                        <b>SinglePlayer</b>
                        <input style={{ display: "inline-block" }} onChange={handleChange} type="checkbox" id="lname" name="singlePlayer" checked={input.singlePlayer} />
                    </label>

                    <label style={{ display: "block", width: "150px" }}>
                        <b>MultiPlayer</b>
                        <input style={{ display: "inline-block" }} onChange={handleChange} type="checkbox" id="lname" name="multiplayer" checked={input.multiplayer} />
                    </label>

                        <br />
                        <input className="input-submit" type="submit" value="Submit" />
                    </form>
                </div>
    
            </>)
}

export default GameForm