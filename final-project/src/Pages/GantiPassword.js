import axios from "axios"
import Cookies from "js-cookie"
import React, { useContext, useState } from "react"
import { useHistory } from "react-router"
import {UserContext} from "../Context/UserContext"

const GantiPassword = () => {
    let history = useHistory()
    const [input,setInput] = useState({
        current_password:"",
        new_password:"",
        new_confirm_password:""
    })
    const handleSubmit = (event) => {
        event.preventDefault()
        console.log(input)
        axios.post('https://backendexample.sanbersy.com/api/change-password',{
            current_password:input.current_password,
            new_password:input.new_password,
            new_confirm_password:input.new_confirm_password
        },
        {headers: {"Authorization" : "Bearer" + Cookies.get('token')}}
        ).then(()=>{
            history.push('/')
        })
    }
    const handleChange = (event) => {
        let typeOfInput = event.target.value
        let name = event.target.name
        setInput({...input, [name]: typeOfInput})
    }
    return (
        <>
            <div className="form">
                    <h1 style={{textAlign:"center"}}>Form Change Password</h1>
                    <form style={{width:"60%", margin:"auto"}}method="post" onSubmit={handleSubmit}>
                        <label >Current Password</label>
                        <input onChange={handleChange} required className="input-form" type="password" id="fname" name="current_password"  value={input.current_password} />
    
                        <label >New Password</label>
                        <input onChange={handleChange} required className="input-form" type="password" id="fname" name="new_password" value={input.new_password} />
    
                        <label >New Confirm Password</label>
                        <input onChange={handleChange} required className="input-form" type="password" id="lname" name="new_confirm_password"  value={input.new_confirm_password} />
                        <br />

                        <input className="input-submit" type="submit" value="Submit" />
                    </form>
                </div>
        </>
    )
}
export default GantiPassword