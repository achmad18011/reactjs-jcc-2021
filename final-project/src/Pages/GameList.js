import React, { useContext, useEffect, useState } from "react"
import { Link, useHistory } from "react-router-dom"
import { GameContext } from "../Context/GamesContext"
import { Table, Tag, Space, Button, Input } from 'antd';
import { Layout, Menu, Breadcrumb } from 'antd';
import { PlusOutlined, EditOutlined, DeleteOutlined } from '@ant-design/icons';
import axios from "axios";


const GameList = () => {
    let history = useHistory()
    const { currentId, setCurrentId, input, setInput, functions } = useContext(GameContext)
    const { functionDelete,
        functionEdit,
        fetchDataId,
        handleChange,
        functionSubmit,
        functionUpdate } = functions

        const [filter, setFilter] = useState({
            release:null,
            platform:"",
            genre: "" 
        })

        const [data, setData] = useState([])
        const [fetchStatus, setFetchStatus] = useState(true)
        const fetchData = async () => {
            let result = await axios.get(`https://backendexample.sanbersy.com/api/data-game`)
            let fetchResult = result.data
            console.log(fetchResult)
            setData(
                fetchResult.map((res) => {
                    return (
                        {
                            id : res.id,
                            genre: res.genre,
                            image_url: res.image_url,
                            singlePlayer: res.singlePlayer, 
                            multiplayer: res.multiplayer,
                            name: res.name,
                            platform: res.platform,
                            release: res.release,
                        }
                    )
    
                })
            )
        }
    useEffect(() => {

        if(fetchStatus){
            fetchData()
            setFetchStatus(false)
        }
    })

    const handleEdit = (e) => {
        let idMobile = parseInt(e.currentTarget.value)
        history.push(`/GameForm/${idMobile}`)
    
    }

    const handleDelete = (e) => {
        let idMobile = parseInt(e.currentTarget.value)

        functionDelete(idMobile)
    }
    const columns = [
        {
          title: 'Name',
          dataIndex: 'name',
          key: 'name',
        },
        {
            title: 'Release Year',
            dataIndex: 'release',
            key: 'release',
          },
          {
            title: 'Platform',
            dataIndex: 'platform',
            key: 'platform',
          },
          {
            title: 'Genre',
            dataIndex: 'genre',
            key: 'genre',
          },
          {
            title: 'SinglePlayer',
            dataIndex: 'singlePlayer',
            key: 'singlePlayer',
          },{
            title: 'Multiplayer',
            dataIndex: 'multiplayer',
            key: 'multiplayer',
          },
          {
            title: 'Image',
            dataIndex: 'image_url',
            key: 'image_url',
            render: text => <img src={text} style={{maxHeight:"120px", maxWidth:"120px"}}/>
          },
          {
            title: 'Action',
            key: 'action',
            render: (text, record) => (
              <Space size="middle">
                <Button onClick={handleEdit} value={text.id}><EditOutlined/></Button>
                <Button type="primary" onClick={handleDelete} value={text.id}><DeleteOutlined/></Button>
              </Space>
            ),
          },
    ]
    const { Search } = Input;
    const onSearch = value => {
        let fetchSearch = async() => {
            let result = await axios.get(`https://backendexample.sanbersy.com/api/data-game`)
            let data = result.data

            let filterResult = data.filter((e)=>{
                return (Object.values(e).join(" ").toLowerCase().includes(value.toLowerCase()))
            })
            setData(filterResult)
        }
        fetchSearch()
    };
    const datas = data
    const handleFilter = (e) => {
        e.preventDefault()
        console.log(filter)
        let fetchFilter = async () => {
            let result = await axios.get(`https://backendexample.sanbersy.com/api/data-game`)
            let data = result.data
            console.log(data)
            let filterData = data.filter((e)=>{
                return e.release == filter.release || e.platform === filter.platform || e.genre === filter.genre
            })
            setData(filterData)
        }
        fetchFilter()
    }
    const handleChangeFilter = (e) => {
        let valueType = e.target.value
        let name = e.target.name

        setFilter({...filter, [name]:valueType})
    }

    return (
        <>
            <Layout>
                <h1 style={{textAlign:"center",fontSize:"large"}}>GAME LIST</h1>
                <br/>
                <div>
                    <form style={{marginLeft:"60px"}} onSubmit={handleFilter}>
                        <Input type="number" onChange={handleChangeFilter} value={filter.release} name="release" placeholder="Release Year" style={{width:"150px",marginRight:"20px"}}/>
                        <Input type="text" onChange={handleChangeFilter} value={filter.platform} name="platform" placeholder="Platform" style={{width:"150px",marginRight:"20px"}}/>
                        <Input type="text" onChange={handleChangeFilter} value={filter.genre} name="genre" placeholder="genre" style={{width:"150px",marginRight:"20px"}}/>
                        <input type="submit" value="Filter"/>
                    </form>
                    <button style={{marginLeft:"60px"}} onClick={()=>{
                            setFetchStatus(true)
                            setFilter({
                                release:null,
                                platform:"",
                                genre: ""
                            })
                        }}>Reset Filter</button>
                </div>
                <br/>
                <Search style={{width:"25%",marginLeft:"60px"}} placeholder="input search text" allowClear enterButton="Search" onSearch={onSearch}/>
                <br/>
                <Button style={{position:"absolute",width:"150px", right:"60px", marginTop:"140px"}}><Link to="/GameForm"><PlusOutlined /> Add Data Game</Link></Button>
            <Table columns={columns} dataSource={datas} style={{width:"90%", margin:"auto"}}/>
            </Layout>
        </>
    )
}
export default GameList