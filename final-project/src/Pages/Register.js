import axios from "axios"
import Cookies from "js-cookie"
import React, { useContext, useState } from "react"
import { useHistory } from "react-router"
import {UserContext} from "../Context/UserContext"

const Register = () => {
    let history = useHistory()
    const [input,setInput] = useState({
        name:"",
        email:"",
        password:""
    })
    const handleSubmit = (event) => {
        event.preventDefault()
        console.log(input)
        axios.post('https://backendexample.sanbersy.com/api/register',{
            name:input.name,
            email:input.email,
            password:input.password
        }).then(()=>{
            history.push('/Login')
        })
    }
    const handleChange = (event) => {
        let typeOfInput = event.target.value
        let name = event.target.name
        setInput({...input, [name]: typeOfInput})
    }
    return (
        <>
            <div className="form">
            <h1 style={{textAlign:"center"}}>REGISTER</h1>
            <form style={{width:"30%", margin:"auto"}}method="post" onSubmit={handleSubmit}>
                <label > Name</label>
                <input onChange={handleChange} required className="input-form" type="text" id="fname" name="name"  value={input.name} />

                <label > Email</label>
                <input onChange={handleChange} required className="input-form" type="text" id="fname" name="email"  value={input.email} />
    
                <label >Password</label>
                <input onChange={handleChange} required className="input-form" type="password" id="fname" name="password" value={input.password} />
                <br />

                <input className="input-submit" type="submit" value="Login" />
            </form>
            </div>
        </>
    )
}
export default Register