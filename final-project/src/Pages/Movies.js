import React, { useContext, useEffect } from "react";
import { MovieContext } from "../Context/MovieContext";
import { Card, Row, Col } from 'antd';
import { useHistory } from "react-router"
import { Button } from 'antd';


const Movies = () =>{
    let history = useHistory()
    const { data, setData, fetchStatus, setFetchStatus, currentId, setCurrentId, input, setInput, functions } = useContext(MovieContext)
    const { fetchData, fetchDataId } = functions


    useEffect(() => {

        if(fetchStatus){
            fetchData()
            setFetchStatus(false)
        }

    },[fetchStatus, setFetchStatus])

    const description = (e) => {
        let value = parseInt(e.currentTarget.value)
        console.log(value)
        setFetchStatus(true)
        history.push(`/Movie/${value}`)
    }


    return (
        <>
            <div className="site-card-wrapper">
            <Row gutter={10}>
                {data !== null && (
                    <>
                        {data.map((e) => {
                            return (
                                <>
                                <div style={{maxWidth: "270px"}}>
                                <Col span={50}>
                                    <Card>
                                    <br/>
                                    <img className="fakeimg" style={{ width: "210px", height: "300px", objectFit: "cover" }} src={e.image_url} />
                                    <h3>{e.title}</h3> 
                                    <Button type="primary" onClick={description} value={e.id}>Read More</Button>
                                    </Card>    
                                </Col>        
                                </div>
                                    
                                </>
                            )
                        })}
                    </>)}
            </Row>
            </div>
        </>
    )
}
export default Movies