import React, { useContext, useEffect } from "react";
import { MovieContext } from "../Context/MovieContext";
import { Card } from 'antd';
import { useHistory, useParams } from "react-router"
import { StarTwoTone } from '@ant-design/icons';


const MoviesDescription = () =>{
    let history = useHistory()
    const { data, setData,dataDescription, setDataDescription, fetchStatus, setFetchStatus, currentId, setCurrentId, input, setInput, functions } = useContext(MovieContext)
    const { fetchData, fetchDataId } = functions
    const {value} = useParams()

    useEffect(() => {

        if(fetchStatus){
            fetchDataId(value)
            setFetchStatus(false)
        }

    },[fetchStatus, setFetchStatus])
    const gridStyle = {
        width: '50%',
      };


    return (
        <>
        <div className="site-card-border-less-wrapper">
            <Card bordered={false} style={{ width: 1000 }}>  
            <Card.Grid hoverable={false} style={gridStyle}>
                <img className="fakeimg" style={{ width: "100%", height: "100%", objectFit: "cover" }} src={dataDescription.image_url} /> 
            </Card.Grid>
            <Card.Grid hoverable={false} style={{width:"50%"}}>
                <h2>{dataDescription.title}</h2>
                <h5>Release Year : {dataDescription.year}</h5>
                <p><b>Genre :</b> {dataDescription.genre}</p>
                <p><b>Rating :</b> {dataDescription.rating} <StarTwoTone/></p>
                <p><b>Review :</b> {dataDescription.review}</p>
                <p><b>Description :</b> {dataDescription.description}</p>
            </Card.Grid>
                
            </Card>
        </div>
                                 
        </>
    )
}
export default MoviesDescription