import axios from "axios"
import React, { createContext, useState } from "react"
import { useHistory } from "react-router"
import Cookies from "js-cookie"

export const MovieContext = createContext()

export const MovieProvider = props => {
    let history = useHistory()
    const [data, setData] = useState([])
    const [dataDescription, setDataDescription] = useState([])
    const [fetchStatus, setFetchStatus] = useState(true)
    const [searchStatus, setSearchStatus] = useState(true)
    const [currentId, setCurrentId] = useState(null)
    const [input, setInput] = useState({
        description: "",
        duration: 0,
        image_url: "",
        title: "",
        review: "",
        rating: 0,
        year: 1980,
        genre: ""
    })

    const fetchData = async () => {
        let result = await axios.get(`https://backendexample.sanbersy.com/api/data-movie`)
        let fetchResult = result.data
        console.log(fetchResult)
        setData(
            fetchResult.map((res) => {
                return {
                    id: res.id,
                    description: res.description,
                    duration: res.duration,
                    image_url: res.image_url,
                    title: res.title,
                    review: res.review,
                    rating: res.rating,
                    year: res.year,
                    genre: res.genre
                }

            })
        )
    }

    const fetchDataId = async (idData) => {
        let result = await axios.get(`https://backendexample.sanbersy.com/api/data-movie/${idData}`)
        let fetchResult = result.data
        console.log(fetchResult)
        setDataDescription(fetchResult)
    }
    const functionSubmit = () => {
        console.log(input)
        axios.post(`https://backendexample.sanbersy.com/api/data-movie`, {
            description: input.description,
            duration: input.duration,
            image_url: input.image_url,
            title: input.title,
            review: input.review,
            rating: input.rating,
            year: input.year,
            genre: input.genre
        },
        {headers: {"Authorization" : "Bearer" + Cookies.get('token')}}
        ).then((res) => {
            console.log(res)
            setFetchStatus(true)
        })
    }

    const functionUpdate = () => {
        axios.put(`https://backendexample.sanbersy.com/api/data-movie/${currentId}`,{
            description: input.description,
            duration: input.duration,
            image_url: input.image_url,
            title: input.title,
            review: input.review,
            rating: input.rating,
            year: input.year,
            genre: input.genre
        },
        {headers: {"Authorization" : "Bearer" + Cookies.get('token')}}
        ).then(() => {
            setFetchStatus(true)
        })
    }

    const functionEdit = (idMobile) => {
            axios.get(`https://backendexample.sanbersy.com/api/data-movie/${idMobile}`)
            .then((result) => {
                let fetchResult = result.data
                setInput(
                    {
                        description: fetchResult.description,
                        duration: fetchResult.duration,
                        image_url: fetchResult.image_url,
                        title: fetchResult.title,
                        review: fetchResult.review,
                        rating: fetchResult.rating,
                        year: fetchResult.year,
                        genre: fetchResult.genre
                    }

                )

                setCurrentId(fetchResult.id)
            })
    }
 
    const functionDelete = (idMobile) => {
        axios.delete(`https://backendexample.sanbersy.com/api/data-movie/${idMobile}`,
        {headers: {"Authorization" : "Bearer" + Cookies.get('token')}}
        )
        .then(() => {
            setFetchStatus(true)
        })
    }

    const handleChange = (e) => {
        let value = e.target.value
        let name = e.target.name
        setInput({ ...input, [name]: value })
    }


    const functions = {
        functionDelete,
        functionEdit,
        fetchData,
        fetchDataId,
        handleChange,
        functionSubmit,
        functionUpdate
    }

    return (
        <MovieContext.Provider value={{
            data,
            setData,
            dataDescription, 
            setDataDescription,
            fetchStatus,
            setFetchStatus,
            searchStatus, 
            setSearchStatus,
            currentId,
            setCurrentId,
            input,
            setInput,
            functions
        }}>
            {props.children}
        </MovieContext.Provider>
    )


}