import axios from "axios"
import React, { createContext, useState } from "react"
import { useHistory } from "react-router"
import Cookies from "js-cookie"

export const GameContext = createContext()

export const GamesProvider = props => {
    let history = useHistory()
    const [data, setData] = useState([])
    const [dataDescription, setDataDescription] = useState([])
    const [fetchStatus, setFetchStatus] = useState(true)
    const [searchStatus, setSearchStatus] = useState(true)
    const [currentId, setCurrentId] = useState(null)
    const [input, setInput] = useState({
        name:"",
        genre:"",
        singlePlayer:true,
        multiplayer:true,
        platform:"",
        release:2000,
        image_url:""
    })

    const fetchData = async () => {
        let result = await axios.get(`https://backendexample.sanbersy.com/api/data-game`)
        let fetchResult = result.data
        console.log(fetchResult)
        setData(
            fetchResult.map((res) => {
                return (
                    {
                        id : res.id,
                        genre: res.genre,
                        image_url: res.image_url,
                        singlePlayer: res.singlePlayer, 
                        multiplayer: res.multiplayer,
                        name: res.name,
                        platform: res.platform,
                        release: res.release,
                    }
                )

            })
        )
    }
    const fetchDataId = async (idData) => {
        let result = await axios.get(`https://backendexample.sanbersy.com/api/data-game/${idData}`)
        let fetchResult = result.data
        console.log(fetchResult)
        setDataDescription(fetchResult)
    }

    const functionSubmit = () => {
        console.log(input)
        axios.post(`https://backendexample.sanbersy.com/api/data-game`, {
            genre: input.genre,
            image_url: input.image_url,
            singlePlayer: input.singlePlayer, 
            multiplayer: input.multiplayer,
            name: input.name,
            platform: input.platform,
            release: input.release,
        },
        {headers: {"Authorization" : "Bearer" + Cookies.get('token')}}
        ).then((res) => {
            console.log(res)
            setFetchStatus(true)})
    }

    const functionUpdate = () => {
        axios.put(`https://backendexample.sanbersy.com/api/data-game/${currentId}`,{
            genre: input.genre,
            image_url: input.image_url,
            singlePlayer: input.singlePlayer, 
            multiplayer: input.multiplayer,
            name: input.name,
            platform: input.platform,
            release: input.release,
        },
        {headers: {"Authorization" : "Bearer" + Cookies.get('token')}}
        ).then((e) => {
            setFetchStatus(true)
            history.push('/mobile-list')
        })
    }

    const functionEdit = (idMobile) => {
            axios.get(`https://backendexample.sanbersy.com/api/data-game/${idMobile}`)
            .then((result) => {
                let fetchResult = result.data
                setInput(
                    {
                        name: fetchResult.name,
                        genre: fetchResult.genre,
                        image_url: fetchResult.image_url,
                        singlePlayer: fetchResult.singlePlayer, 
                        multiplayer: fetchResult.multiplayer,
                        platform: fetchResult.platform,
                        release: fetchResult.release,
                    }

                )
                setCurrentId(fetchResult.id)
            })
    }
 
    const functionDelete = (idMobile) => {
        axios.delete(`https://backendexample.sanbersy.com/api/data-game/${idMobile}`,
        {headers: {"Authorization" : "Bearer" + Cookies.get('token')}}
        )
        .then(() => {
            setFetchStatus(true)
        })
    }

    const handleChange = (e) => {
        let value = e.target.value
        let name = e.target.name
        let player = ["singlePlayer", "multiplayer"]

        if (player.indexOf(name) === -1) {
            setInput({ ...input, [name]: value })
        } else {
            setInput({ ...input, [name]: !input[name] })
        }

    }


    const functions = {
        functionDelete,
        functionEdit,
        fetchData,
        fetchDataId,
        handleChange,
        functionSubmit,
        functionUpdate
    }

    return (
        <GameContext.Provider value={{
            data,
            setData,
            dataDescription, 
            setDataDescription,
            fetchStatus,
            setFetchStatus,
            searchStatus, 
            setSearchStatus,
            currentId,
            setCurrentId,
            input,
            setInput,
            functions
        }}>
            {props.children}
        </GameContext.Provider>
    )


}