// soal 1
console.log("-------- Soal 1 --------")
var kataPertama = "saya";
var kataKedua = "senang";
var kataKetiga = "belajar";
var kataKeempat = "javascript";

var ubah2 = kataKedua[0].toUpperCase() + kataKedua.substr(1)
var ubah3 = kataKetiga.substr(0, 6) + kataKetiga[6].toUpperCase()
var ubah4 = kataKeempat.toUpperCase()
console.log(kataPertama + " " + ubah2 + " " + ubah3 + " " +ubah4)

// soal 2
console.log("-------- Soal 2 --------")
var panjangPersegiPanjang = "8";
var lebarPersegiPanjang = "5";

var alasSegitiga= "6";
var tinggiSegitiga = "7";

var p = parseInt(panjangPersegiPanjang)
var l = parseInt(lebarPersegiPanjang)
var a = parseInt(alasSegitiga)
var t = parseInt(tinggiSegitiga)

var kelilingPersegiPanjang = (2*p) + (2*l)
var luasSegitiga = 0.5*a*t

console.log("Keliling Persegi Panjang: " + kelilingPersegiPanjang)
console.log("Luas Segitiga: " + luasSegitiga)

// soal 3
console.log("-------- Soal 3 --------")
var sentences= 'wah javascript itu keren sekali'; 

var firstWord= sentences.substr(0, 3); 
var secondWord= sentences.substr(4, 10);
var thirdWord= sentences.substr(15, 3);
var fourthWord= sentences.substr(19, 5);
var fifthWord= sentences.substr(25, 6);

console.log('Kata Pertama: ' + firstWord); 
console.log('Kata Kedua: ' + secondWord); 
console.log('Kata Ketiga: ' + thirdWord); 
console.log('Kata Keempat: ' + fourthWord); 
console.log('Kata Kelima: ' + fifthWord);

// soal 4
console.log("-------- Soal 4 --------")
var nilaiJhon = 80;
var nilaiDoe = 50;

console.log("nilai Jhon: ")
if (nilaiJhon >= 80){
    console.log("indeksnya A")
} else if (nilaiJhon < 80 && nilaiJhon >= 70){
    console.log("indeksnya B")
} else if (nilaiJhon < 70 && nilaiJhon >=60){
    console.log("indeksnya C")
} else if (nilaiJhon < 60 && nilaiJhon >=50){
    console.log("indeksnya D")
} else {
    console.log("indeksnya E")
}
console.log("nilai Doe: ")
if (nilaiDoe >= 80){
    console.log("indeksnya A")
} else if (nilaiDoe < 80 && nilaiDoe >= 70){
    console.log("indeksnya B")
} else if (nilaiDoe < 70 && nilaiDoe >= 60){
    console.log("indeksnya C")
} else if (nilaiDoe < 60 && nilaiDoe >= 50){
    console.log("indeksnya D")
} else {
    console.log("indeksnya E")
}

// soal 5
console.log("-------- Soal 5 --------")
var tanggal = 9;
var bulan = 10;
var tahun = 1998;

switch(bulan){
    case 1: {console.log(tanggal + " Januari " + tahun);break;}
    case 2: {console.log(tanggal + " Februari " + tahun);break;}
    case 3: {console.log(tanggal + " Maret " + tahun);break;}
    case 4: {console.log(tanggal + " April " + tahun);break;}
    case 5: {console.log(tanggal + " Mei " + tahun);break;}
    case 6: {console.log(tanggal + " Juni " + tahun);break;}
    case 7: {console.log(tanggal + " Juli " + tahun);break;}
    case 8: {console.log(tanggal + " Agustus " + tahun);break;}
    case 9: {console.log(tanggal + " September " + tahun);break;}
    case 10: {console.log(tanggal + " Oktober " + tahun);break;}
    case 11: {console.log(tanggal + " November " + tahun);break;}
    case 12: {console.log(tanggal + " Desember " + tahun);break;}
}