// soal 1
console.log("--------1--------")
const luasLingkaran = (r) => {
    return (Math.PI*r*r);
}
const kelilingLingkaran = (r) => {
    return (2*Math.PI*r);
}

let r = 7
const luas = luasLingkaran(r)
const keliling = kelilingLingkaran(r)
console.log(luas)
console.log(keliling)

// soal 2
console.log("--------2--------")
const introduce = (...rest) =>{
    let [nama,umur,jenisKelamin,kerjaan] = rest
    return `Pak ${nama} adalah seorang ${kerjaan} yang berusia ${umur} tahun`
}
const perkenalan = introduce("John", "30", "Laki-Laki", "penulis")
console.log(perkenalan)

// soal 3
console.log("--------3---------")
const newFunction = (firstName, lastName) => {
    return {
        firstName,
        lastName,
        fullName: function(){
            console.log(firstName + " " + lastName)
        }
    }
}
console.log(newFunction("John", "Doe").firstName)
console.log(newFunction("Richard", "Roe").lastName)
newFunction("William", "Imoh").fullName()

// soal 4
console.log("--------4--------")
let phone = {
    name: "Galaxy Note 20",
    brand: "Samsung",
    year: 2020,
    colors: ["Mystic Bronze", "Mystic White", "Mystic Black"]
 }
 
const {name, brand, year, colors} = phone
const phoneName = name
const phoneBrand = brand
const [colorBronze, colorWhite, colorBlack] = colors

console.log(phoneBrand, phoneName, year, colorBlack, colorBronze) 

// soal 5
console.log("--------5--------")
let warna = ["biru", "merah", "kuning" , "hijau"]

let dataBukuTambahan= {
  penulis: "john doe",
  tahunTerbit: 2020 
}

let buku = {
  nama: "pemograman dasar",
  jumlahHalaman: 172,
  warnaSampul:["hitam"]
}

let combinedWarna = [...buku.warnaSampul, ...warna]
let dataBuku = {nama:buku.nama, jumlahHalaman:buku.jumlahHalaman, warnaSampul:combinedWarna, ...dataBukuTambahan}
console.log(dataBuku)