// soal 1
console.log("--------1--------")
// Release 0
console.log("---Release 0----")
class Animal {
    constructor(nama){
        this.name = nama;
        this._legs = 4;
        this.cold_blooded = false;
    }
    get legs(){
        return this._legs;
    }
    set legs(x){
        this._legs = x;
    }
}
var sheep = new Animal("shaun");
 
console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false
sheep.legs = 3
console.log(sheep.legs)

// Release 1
console.log("---Release 1----")
class Ape extends Animal{
    constructor (nama){
        super(nama);
        this._legs = 2;
    }
    yell(){
        console.log("Auooo")
    }
}
class Frog extends Animal{
    constructor (nama){
        super(nama);
        this._legs = 4;
    }
    jump(){
        console.log("hop hop")
    }
}
var sungokong = new Ape("kera sakti")
sungokong.yell() // "Auooo"
sungokong.legs = 2
console.log(sungokong.name)
console.log(sungokong.legs)
console.log(sungokong.cold_blooded)

var kodok = new Frog("buduk")
kodok.jump() // "hop hop"
console.log(kodok.name)
console.log(kodok.legs)
console.log(kodok.cold_blooded)

// soal 2 
console.log("--------2--------")
class Clock {
    constructor({ template }){
        var timer;
        this.render=function(){
            var date = new Date();
        
            var hours = date.getHours();
            if (hours < 10) hours = '0' + hours;
        
            var mins = date.getMinutes();
            if (mins < 10) mins = '0' + mins;
        
            var secs = date.getSeconds();
            if (secs < 10) secs = '0' + secs;
        
            var output = template
              .replace('h', hours)
              .replace('m', mins)
              .replace('s', secs);
        
            console.log(output);
        }
        this.stop=function() {
            clearInterval(timer);
        };
        this.start=function() {
            this.render();
            timer = setInterval(this.render, 1000);
        };
    }
}
var clock = new Clock({template: 'h:m:s'});
clock.start();  