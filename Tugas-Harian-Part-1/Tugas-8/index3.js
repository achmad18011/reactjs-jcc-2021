var filterBooksPromise = require('./promise2.js')
let a = 250
let b = 40
let c = 30
const execute = (color,page) => {
    filterBooksPromise(color,page)
        .then((res)=>{
            console.log(res)
        })
        .catch((err)=>{
            console.log(err)
        })
}
const execute2 = async (color,page) => {
    try {
        let output = await filterBooksPromise(color, page)
        console.log(output)
    }catch (err) {
        console.log(err.message)
    }

}
execute(true,b)
execute2(false,a)
execute2(true,c)
