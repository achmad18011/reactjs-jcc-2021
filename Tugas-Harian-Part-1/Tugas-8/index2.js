var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
let waktu = 10000
let b = 0
const execute = (time,book) => {
    readBooksPromise(time,book)
        .then(function(res){
            if (b < (books.length-1)){
                b+=1
                execute(res, books[b])
            }
        })
        .catch((err)=>{
            console.log(err)
        })
}
execute(waktu,books[b])