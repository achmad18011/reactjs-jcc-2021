// soal 1
console.log("--------1--------")
function luasPersegiPanjang(panjang, lebar){
    return panjang*lebar
}
function kelilingPersegiPanjang(panjang, lebar){
    return 2*(panjang+lebar)
}
function volumeBalok(panjang, lebar, tinggi){
    return panjang*lebar*tinggi
}
var panjang= 12
var lebar= 4
var tinggi = 8
 
var luasPersegiPanjang = luasPersegiPanjang(panjang, lebar)
var kelilingPersegiPanjang = kelilingPersegiPanjang(panjang, lebar)
var volumeBalok = volumeBalok(panjang, lebar, tinggi)

console.log(luasPersegiPanjang) 
console.log(kelilingPersegiPanjang)
console.log(volumeBalok)

// soal 2
console.log("--------2--------")
function introduce(nama, age, address, hobby){
    return ("Nama saya " + nama + ", umur saya " + age + " tahun, alamat saya di " + address + " dan saya punya hobby yaitu " + hobby + "!") 
}
var name = "John"
var age = 30
var address = "Jalan belum jadi"
var hobby = "Gaming"
 
var perkenalan = introduce(name, age, address, hobby)
console.log(perkenalan)

// soal 3
console.log("--------3--------")
var arrayDaftarPeserta = ["John Doe", "laki-laki", "baca buku" , 1992]
var peserta = {}
peserta.nama = arrayDaftarPeserta[0]
peserta.jenisKelamin = arrayDaftarPeserta[1]
peserta.hobi = arrayDaftarPeserta[2]
peserta.tahunLahir = arrayDaftarPeserta[3]
console.log(peserta)

// soal 4
console.log("--------4--------")
namaBuah = ["Nanas", "Jeruk", "Semangka", "Pisang"]
warnaBuah = ["Kuning", "Oranye", "Hijau & Merah", "Kuning"]
biji = [false, true, true, false]
harga = [9000, 8000, 10000, 5000]
var obj = [{},{},{},{}]
for (var i = 0; i<4; i++){
    obj[i].nama = namaBuah[i]
    obj[i].warna = warnaBuah[i]
    obj[i].adaBijinya = biji[i]
    obj[i].harga = harga[i]
}
var array = obj.filter(function(item){
    return item.adaBijinya == false;
})
console.log(array)

// soal 5
console.log("--------5--------")
function tambahDataFilm(a, b, c, d){
    var objek = {nama:a, durasi:b, genre:c, tahun:d}
    return dataFilm.push(objek)
}
var dataFilm = []

tambahDataFilm("LOTR", "2 jam", "action", "1999")
tambahDataFilm("avenger", "2 jam", "action", "2019")
tambahDataFilm("spiderman", "2 jam", "action", "2004")
tambahDataFilm("juon", "2 jam", "horror", "2004")
console.log(dataFilm)
