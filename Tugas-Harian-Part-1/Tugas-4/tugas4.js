// soal 1
console.log("---------1--------")
console.log("LOOPING PERTAMA")
var loop1 = 1;
while (loop1 <= 10){
    console.log(2*loop1 + " - I love coding");
    loop1++;
}
console.log("LOOPING KEDUA")
var loop1 = 10;
while (loop1 > 0){
    console.log(2*loop1 + " - I will become a frontend developer");
    loop1--;
}

// soal 2
console.log("--------2--------")
for (var loop2 = 1; loop2 <= 20; loop2++){
    if (loop2%2 != 0){
        if (loop2%3 == 0){
            console.log(loop2 + " - I Love Coding")
        }else {
            console.log(loop2 + " - Santai")
        }
    } else{
        console.log(loop2 + " - Berkualitas")
    }
}

// soal 3
console.log("--------3--------")
var huruf = "";
for(var loop3 = 1; loop3 <= 7; loop3++){
    huruf = huruf.concat("#")
    console.log(huruf)
}

// soal 4
console.log("--------4--------")
var kalimat=["aku", "saya", "sangat", "sangat", "senang", "belajar", "javascript"]
kalimat.splice(0,1)
kalimat.splice(2,1)
var kalimat_baru = kalimat.join(" ")
console.log(kalimat_baru)

// soal 5
console.log("--------5--------")
var sayuran=[];
sayuran.splice(0,0,"Kangkung","Bayam","Buncis","Kubis","Timun","Seledri","Tauge")
sayuran.sort()
for(var i =1; i <= sayuran.length; i++){
    console.log(i + ". " + sayuran[i-1])
}