import './App.css';
import { MobileListProvider } from './Context/Context';
import Routes from './layout/Body';
import MobileList from './layout/Mobile-list';
import 'antd/dist/antd.css';


function App() {
  return (
    <>
    <MobileListProvider>
      <Routes/>
    </MobileListProvider>
    <footer>
        <h5>&copy; Quiz 3 ReactJS Sanbercode</h5>
    </footer>
    </>
  );
}

export default App;
