import React from "react"
import { BrowserRouter as Router,Switch, Route } from "react-router-dom"
import About from "./About";
import Home from './Home';
import MobileForm from "./Mobile-Form";
import MobileList from "./Mobile-list";
import Nav from './Nav';
import Search from "./Search";

const Routes = () => {
    return(
        <>
        <Router>
            <Nav/>
            <Switch>
                <Route exact path="/">
                    <Home/>
                </Route>
                <Route exact path="/MobileList">
                    <MobileList/>
                </Route>
                <Route exact path="/MobileForm">
                    <MobileForm/>
                </Route>
                <Route exact path="/MobileForm/:value">
                    <MobileForm/>
                </Route>
                <Route exact path="/Search/:value">
                    <Search/>
                </Route>
                <Route exact path="/About">
                    <About/>
                </Route>
            </Switch>
            
        </Router>
        </>
    )
}
export default Routes