import React, {useState, useEffect, useContext} from "react"
import axios from "axios"
import { Link } from "react-router-dom"
import { useHistory, useParams } from "react-router"
import { MobileListiContext } from "../Context/Context"
import { Table, Tag, Space } from 'antd';
import { message, Button, notification } from 'antd';
import { DeleteOutlined, EditOutlined, PlusCircleOutlined } from '@ant-design/icons';

const MobileList = () => {
  
    let history = useHistory()
    const {MobileList, setMobileList, input, setInput, fetchStatus, setFetchStatus, currentId, setCurrentId, functions} = useContext (MobileListiContext)
    const {fetchData, fetchDataId, Price,functionDelete, functionEdit, functionSubmit, functionUpdate} = functions
    useEffect( () => {
    
        if(fetchStatus){
            fetchData()
            setFetchStatus(false)
        }  
        
      }, [fetchStatus,setFetchStatus])
      const success = () => {
        message.success('Data Terhapus', 3);
      };
      const handleEdit = (event) => {
        let idData = parseInt(event.currentTarget.value)
        history.push(`/Mobileform/${idData}`)
        functionEdit(idData)
    }

    const handleDelete = (event) => {
        let idData = parseInt(event.currentTarget.value)
        functionDelete(idData)
        success()
    }
      const columns = [
        {
          title: 'Nama',
          dataIndex: 'name',
          key: 'name',
        },
        {
          title: 'Category',
          dataIndex: 'category',
          key: 'category',
        },
        {
            title: 'Description',
            dataIndex: 'description',
            key: 'description',
        },
        {
            title: 'Realease Year',
            dataIndex: 'release_year',
            key: 'release_year',
        },
        {
            title: 'Size',
            dataIndex: 'size',
            key: 'size',
        },
        {
            title: 'Price',
            dataIndex: 'price',
            key: 'price',
          },
          {
            title: 'Rating',
            dataIndex: 'rating',
            key: 'rating',
          },
          {
            title: 'Platform',
            dataIndex: 'platform',
            key: 'platform',
          },
          {
            title: 'Action',
            key: 'action',
            render: (text, index) => (
              <Space size="middle">
                <Button onClick={handleEdit} value={text.id}><EditOutlined /></Button>
                <Button onClick={handleDelete} style={{backgroundColor:"red"}}  value={text.id}><DeleteOutlined /></Button>
              </Space>
            ),
          },
    ]
    const data = MobileList
    return (
      <>
        <h1 style={{textAlign:"center"}}>Mobile Apps List</h1>
        <br/>
        <Link to="/MobileForm"><Button style={{marginLeft:"270px", backgroundColor:"blue", color:"white"}}><PlusCircleOutlined />Tambah Data</Button></Link>
        <Table columns={columns} dataSource={data} style={{width:"60%", margin:"auto"}}/>
      </>
    )
  }
    

export default MobileList