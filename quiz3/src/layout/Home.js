import React, { useContext, useEffect } from "react";
import { MobileListiContext } from "../Context/Context";

const Home = () =>{
    const {MobileList, setMobileList, input, setInput, fetchStatus, setFetchStatus, currentId, setCurrentId, functions} = useContext (MobileListiContext)
    const {fetchData, fetchDataId, Price, Size} = functions
    useEffect( () => {
    
        if(fetchStatus){
            fetchData()
            setFetchStatus(false)
        }  
        
      }, [fetchStatus,setFetchStatus])
    return (
    <>
        {MobileList.map((item)=>{
            return (
                <div class="row">
        <div class="section">
            <div class="card">
                <div>
                    <h2>{item.name}</h2>
                    <h5>Release Year : {item.release_year}</h5>
                    <img className="fakeimg" style={{width:" 50%", height: "300px",}} src={item.image_url} />
                    <br />
                    <br />
                    <div>
                        <strong>Price: {Price(item.price)}</strong><br />
                        <strong>Rating: {item.rating}</strong><br />
                        <strong>Size: {Size(item.size)}</strong><br />
                        <strong style={{marginRight:"10px"}}>Platform : {item.platform}
                        </strong>
                        <br />
                    </div>
                    <p>
                        <strong style={{marginRight: "10px"}}>Description : {item.description}</strong>
    
                    </p>

                    <hr />
                </div>
            </div>

        </div>
    </div>
            )
        })}
    </>
    )
}

export default Home