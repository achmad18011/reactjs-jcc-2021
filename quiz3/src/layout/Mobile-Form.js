import React, { useContext, useEffect } from "react";
import { useHistory, useParams } from "react-router";
import { MobileListiContext } from "../Context/Context";

const MobileForm = () => {
    let history = useHistory()
    const {MobileList, setMobileList, input, setInput, fetchStatus, setFetchStatus, currentId, setCurrentId, functions} = useContext (MobileListiContext)
    const {fetchData, fetchDataId, Price,functionDelete, functionEdit, functionSubmit, functionUpdate} = functions
    let { value } = useParams()
    useEffect (()=>{
        if(fetchStatus){
            fetchDataId(value)
            setFetchStatus(false)
        }
    },[fetchDataId,fetchStatus,setFetchStatus])
    const handleChange = (event) => {
        let value = event.target.value
        let nama = event.target.name
        setInput({...input,[nama] : value})
        }
    const handleSubmit = (event) =>{
        event.preventDefault()
        if (currentId === null){
            functionSubmit()
        }else {
            functionUpdate()
        }
        setInput({
            name:"",
            category: "",
            description: "",
            size:0,
            price:0,
            rating:0,
            release_year:2007,
            image_url:"",
            is_android_app: true,
            is_ios_app: true,
            platform:""
            })
        setCurrentId(null)
        history.push('/MobileList')
    }
    return(
        <>
        <h1 style={{textAlign:"center"}}>Daftar Nilai Form</h1>
        <form style={{width:"25%",margin:"auto"}} onSubmit = {handleSubmit}>
            <label>Nama:</label>
            <br/>
            <input style={{width:"300px"}} type="text" name="name" value={input.name} onChange={handleChange} required/>
            <br/>
            <label>Category:</label>
            <br/>
            <input style={{width:"300px"}} type="text" name="category" value={input.category} onChange={handleChange} required/>
            <br/>
            <label>Description:</label>
            <br/>
            <input style={{width:"300px"}} type="textarea" name="description" value={input.description}  onChange={handleChange}/>
            <br/>
            <label>Release Year:</label>
            <br/>
            <input type="number" name="release_year" value={input.release_year} min={2007} max={2021} onChange={handleChange} required/>
            <br/>
            <label>Size(MB):</label>
            <br/>
            <input type="number" name="size" value={input.size} min={0} onChange={handleChange} required/>
            <br/>
            <label>Price:</label>
            <br/>
            <input type="number" name="price" value={input.price} min={0} onChange={handleChange} required/>
            <br/>
            <label>Rating:</label>
            <br/>
            <input style={{width:"300px"}} type="text" name="rating" value={input.rating} onChange={handleChange} required/>
            <br/>
            <label>Image URL:</label>
            <br/>
            <input style={{width:"300px"}} type="text" name="image_url" value={input.image_url} onChange={handleChange} required/>
            <br/>
            <label>Platform:</label>
            <br/>
            <input type="checkbox" name="is_android_app" value={input.is_android_app} onChange={handleChange} /> Android
            <br/>
            <input type="checkbox" name="is_ios_app" value={input.is_ios_app} onChange={handleChange}/> IOS
            <br/>
            <input type="submit" value="Submit"/>
        </form>
        </>
    )
}
export default MobileForm