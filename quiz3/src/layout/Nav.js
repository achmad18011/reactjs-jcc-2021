import { Link } from "react-router-dom"
import logo from '../logo.png';
import { message, Button, notification } from 'antd';
import { useContext } from "react";
import { MobileListiContext } from "../Context/Context";
import { useHistory, useParams } from "react-router";

const Nav =()=>{
    let history = useHistory()
    const {MobileList, setMobileList, name, setName, input, setInput, fetchStatus, setFetchStatus, currentId, setCurrentId, functions} = useContext (MobileListiContext)
    const {fetchData, fetchDataId, Price,functionDelete, functionEdit, functionSubmit, functionUpdate, search} = functions

    const handleSearch = () => {
        history.push(`/Search/:${name}`)
    }
    const handleChange = (event) => {
        setName(event.target.value) 
        }
    return(
        <>
        <div class="topnav">
            <a href="">
            <img src={logo} style={{width:"70px"}}/>
            </a>
            <Link to="/">Home</Link>
            <Link to="/MobileList">Movie List</Link>
            <Link to="/About">About</Link>
            <form onSubmit={handleSearch}>
                <input  type="text" value={name} onChange={handleChange} />
                <input type="submit" value="Cari" />
            </form>
        </div>
        </>
    )
}

export default Nav