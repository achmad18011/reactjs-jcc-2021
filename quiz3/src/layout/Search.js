import React, { useContext, useEffect } from "react";
import { useParams } from "react-router";
import { MobileListiContext } from "../Context/Context";
import { useHistory } from "react-router";

const Search = () => {
    const {MobileList, setMobileList, name, setName, input, setInput, fetchStatus, setFetchStatus, currentId, setCurrentId, functions, Mobile, setMobile} = useContext (MobileListiContext)
    const {fetchData, fetchDataId,Size, Price,functionDelete, functionEdit, functionSubmit, functionUpdate, search} = functions
    let { value } = useParams()
    useEffect (()=>{
        if(fetchStatus){
            search(name)
            setFetchStatus(false)
        }
    },[search,fetchStatus,setFetchStatus])
    return(
        <>
                <div class="row">
                <div class="section">
            <div class="card">
                <div>
                    <h2>{Mobile.name}</h2>
                    <h5>Release Year : {Mobile.release_year}</h5>
                    <img className="fakeimg" style={{width:" 50%", height: "300px",}} src={Mobile.image_url} />
                    <br />
                    <br />
                    <div>
                        <strong>Price: {Price(Mobile.price)}</strong><br />
                        <strong>Rating: {Mobile.rating}</strong><br />
                        <strong>Size: {Size(Mobile.size)}</strong><br />
                        <strong style={{marginRight:"10px"}}>Platform : {Mobile.platform}
                        </strong>
                        <br />
                    </div>
                    <p>
                        <strong style={{marginRight: "10px"}}>Description : {Mobile.description}</strong>
    
                    </p>

                    <hr />
                </div>
            </div>

        </div>
    </div>
            )
        
        </>
    )
}
export default Search