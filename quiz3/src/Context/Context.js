import axios from "axios"
import React, { createContext, useState } from "react"

export const MobileListiContext = createContext()

export const MobileListProvider = props => {
    const [MobileList, setMobileList] =  useState([])
    const [name, setName] = useState("")
    const [Mobile, setMobile] = useState([])
    const[input,setInput]=useState({
    name:"",
    category: "",
    description: "",
    size:0,
    price:0,
    rating:0,
    release_year:2007,
    image_url:"",
    is_android_app: true,
    is_ios_app: true,
    platform:""
    })
    const [fetchStatus, setFetchStatus] = useState(true)
    const [currentId, setCurrentId] =  useState(null)
    const Platform = () => {
        if (MobileList.is_android_app == true && MobileList.is_ios_app == true){
            return("Android & IOS")
        }else if(MobileList.is_android_app === true && MobileList.is_ios_app === false){
            return("Android")
        }else if (MobileList.is_android_app === false && MobileList.is_ios_app === true){
            return("IOS")
        }else{
            return("")
        }
    }
    const Price = (value) =>{
        if (value ===0){
            return("Free")
        }else{
            return(`Rp. ${value},-`)
        }
    }
    const Size = (value) => {
        if (value <= 1000){
            return(`${value} MB`)
        }else {
            return(`${value/1000} GB`)
        }
    }

    const fetchData = async () => {
        const result = await axios.get(`http://backendexample.sanbercloud.com/api/mobile-apps`)
        console.log(result)
        let platform = Platform()
        setMobileList(result.data.map(x=>{ 
            return {id: x.id, name: x.name, category: x.category , description: x.description, size:x.size,
                price:x.price,
                rating:x.rating, release_year: x.release_year, is_android_app: x.is_android_app, is_ios_app: x.is_ios_app, platform: platform, image_url: x.image_url  } }) )
      }
      const fetchDataId = async (idData) => {
        const x = await axios.get(`http://backendexample.sanbercloud.com/api/mobile-apps/${idData}`)
        let platform = Platform()
        setInput( {id: x.id, name: x.name, category: x.category , description: x.description, size:x.size,
            price:x.price,
            rating:x.rating, release_year: x.release_year, is_android_app: x.is_android_app, is_ios_app: x.is_ios_app, platform: platform, image_url: x.image_url    } ) 
        setCurrentId(idData)
      }
      const functionDelete = (idData) => {
        axios.delete(`http://backendexample.sanbercloud.com/api/mobile-apps/${idData}`)
        .then(()=>{
            setFetchStatus(true)
        })
    }
    const functionSubmit = () => {
        let name = input.name
        let category = input.category
        let description = input.description
        let size = input.size
        let price = input.price
        let rating = input.rating
        let release_year = input.release_year
        let image_url = input.image_url
        let is_android_app = input.is_android_app
        let is_ios_app = input.is_ios_app
        axios.post(`http://backendexample.sanbercloud.com/api/mobile-apps`,{name,description,category, size, price, rating,image_url, release_year,is_android_app,is_ios_app })
            .then(() => {
                setFetchStatus(true)
            })
    }
    const functionUpdate = () => {
        let name = input.name
        let category = input.category
        let description = input.description
        let size = input.size
        let price = input.price
        let rating = input.rating
        let release_year = input.release_year
        let image_url = input.image_url
        let is_android_app = input.is_android_app
        let is_ios_app = input.is_ios_app
        axios.put(`http://backendexample.sanbercloud.com/api/mobile-apps`,{name,description,category, size, price, rating,image_url, release_year,is_android_app,is_ios_app })
            .then(() => {
                setFetchStatus(true)
            })
    }
    const functionEdit = (idData) => {
        axios.get(`http://backendexample.sanbercloud.com/api/mobile-apps/${idData}`)
        .then((e)=>{
            let data = e.data
            setInput(data)
        })
        setCurrentId(idData)
    }

    const search = (name) =>{
        let cari = MobileList.filter((e)=>{
            return e.name === name
        })
        const x = axios.get(`http://backendexample.sanbercloud.com/api/mobile-apps/${cari.id}`)
        let platform = Platform()
        setMobile( {id: x.id, name: x.name, category: x.category , description: x.description, size:x.size,
            price:x.price,
            rating:x.rating, release_year: x.release_year, is_android_app: x.is_android_app, is_ios_app: x.is_ios_app, platform: platform, image_url: x.image_url    } )
            console.log(x)
    }

    
    const functions = {
        fetchData,
        fetchDataId,
        Price,
        Size, 
        functionDelete,
        functionEdit,
        functionSubmit,
        functionUpdate,
        search
    }

    return(
        <MobileListiContext.Provider value = {{
            MobileList,
            setMobileList,
            name,
            setName,
            input,
            setInput,
            fetchStatus,
            setFetchStatus,
            currentId,
            setCurrentId,
            functions,
            Mobile,
            setMobile
        }}>
            {props.children}
        </MobileListiContext.Provider>
    )
    }